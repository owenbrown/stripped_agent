### Sample folder structure 
**Note that we've moved the data out of the "output" folder, to the root of the repo
```
  / 
|
└──Joke for Kids
           └─ CHANGELOG.md
           └─ stripped_files
└──Horoscope
           └─ CHANGELOG.md
           └─ stripped_files
```
### Example output for `Jokes for Kids/CHANGELOG.md` 

### [2019-03-16T18:49:29.595051](https://docs.google.com/document/d/1d9SyJN-4QwrTNI7uf1OGx2m9BRhyVjUdonwYBCabWRE/edit)
[Upload Version](https://docs.google.com/document/d/1d9SyJN-4QwrTNI7uf1OGx2m9BRhyVjUdonwYBCabWRE/edit)
#### Comments
-  Jean - Finishes `#2342333`. I added intent to quit updates. No change to the webhook.
#### Updated Intents 
- Created 3 new intents
  - Created intent "Cancel updates"
  - Created intent "Re-enroll for push notifications"
  - Created intent "Another joke, similar category"

### [2019-03-16T18:58:18.169647](https://docs.google.com/document/d/1d9SyJN-4QwrTNI7uf1OGx2m9BRhyVjUdonwYBCabWRE/edit)
[Upload Version](https://docs.google.com/document/d/1d9SyJN-4QwrTNI7uf1OGx2m9BRhyVjUdonwYBCabWRE/edit)
#### Comments
Owen - Finished issue `#3842332`. Note - this requires Fulfillment Webhook version 3.2.0 or above. 
#### Updated Intents 
- Edited 2 intents
  - Edited intent "Another Joke"
    - Added 2 training phrases
    - changed action from `joke` to `another joke`
  - Edited intent "end conversation"
    - Added three training phrases
    - Set intent to end conversation

#### Updated entities
- Edited entry "Joke categories"
  - Added entity-entry "Car" , with 22 synonyms
  - Added entity-entry "Animal", adding synonym "leopard"

------
Only updated CHANGELOG.md if that particular agent changed shape

# Work on this iteratively 
1. All CHANGELOG.md files record "intents updated" if any intent was updated.
2. 
